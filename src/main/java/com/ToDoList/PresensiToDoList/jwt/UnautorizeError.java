package com.ToDoList.PresensiToDoList.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// Alur pembuatan 11
@Component
public class UnautorizeError implements AuthenticationEntryPoint {
//    masalah yang umumnya terjadi di sisi klien, biasanya ketika Anda mengakses URL lalu browser Anda mengirimkan permintaan tapi permintaan tersebut tidak diverifikasi

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE); // untuk mengkosongkan respon seblmnya
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // merubah status
        final Map<String, Object> body = new HashMap<>();
        body.put("status" , HttpServletResponse.SC_UNAUTHORIZED);
        body.put("error" , "Unauthorized");
        body.put("message" , authException.getMessage());
        final ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getOutputStream(),body);
    }
}
