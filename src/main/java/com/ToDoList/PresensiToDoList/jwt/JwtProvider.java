package com.ToDoList.PresensiToDoList.jwt;

import com.ToDoList.PresensiToDoList.Exception.InternalErrorException;
import com.ToDoList.PresensiToDoList.Exception.NotFoundException;
import com.ToDoList.PresensiToDoList.model.TemporaryToken;
import com.ToDoList.PresensiToDoList.model.User;
import com.ToDoList.PresensiToDoList.repository.TemporaryTokenRepository;
import com.ToDoList.PresensiToDoList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;


// Alur pembuatan 10
@Component
public class JwtProvider {

//    JSON Web Token dapat berfungsi untuk sistem otentikasi dan juga untuk pertukaran informasi

    private static String seccetKey = "belajar Spring";

    private static Integer expired = 900000;

    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;

    @Autowired
    private UserRepository userRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("_","");
        User user = userRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User not found generate token"));
        var chekingToken = temporaryTokenRepository.findByUserId(user.getId());
        if (chekingToken.isPresent())  temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(user.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error parse"));
    }

    public  boolean checkingTokenJwt(String token) {
        System.out.println(token);
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null ) {
            System.out.println("Token kosong");
            return  false;
        }

        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }
}
