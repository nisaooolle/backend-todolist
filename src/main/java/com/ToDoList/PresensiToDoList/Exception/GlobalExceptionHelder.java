package com.ToDoList.PresensiToDoList.Exception;

import com.ToDoList.PresensiToDoList.response.ResponseHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

// Alur pembuatan 13
@ControllerAdvice
public class GlobalExceptionHelder {
    //    menghandle error
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFound(NotFoundException notFoundException) {
        return ResponseHelper.error(notFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EmailException.class)
    public ResponseEntity<?> emailException(EmailException emailException) {
        return ResponseHelper.error(emailException.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
