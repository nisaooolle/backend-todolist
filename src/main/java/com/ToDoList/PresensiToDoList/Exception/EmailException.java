package com.ToDoList.PresensiToDoList.Exception;

// Alur pembuatan 12
public class EmailException extends RuntimeException{
    //    menghandle error
    public EmailException(String message) {
        super(message);
    }
}
