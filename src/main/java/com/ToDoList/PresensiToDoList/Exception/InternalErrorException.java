package com.ToDoList.PresensiToDoList.Exception;

// Alur pembuatan 14
public class InternalErrorException extends RuntimeException {
    //    menghandle error
    public InternalErrorException(String message) {
        super(message);
    }

}
