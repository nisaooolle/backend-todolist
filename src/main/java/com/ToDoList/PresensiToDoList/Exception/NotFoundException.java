package com.ToDoList.PresensiToDoList.Exception;

// Alur pembuatan 15
public class NotFoundException extends RuntimeException{
    //    menghandle error
    public NotFoundException(String message) {
        super(message);
    }
}
