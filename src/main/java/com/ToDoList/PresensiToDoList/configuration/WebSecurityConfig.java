package com.ToDoList.PresensiToDoList.configuration;

import com.ToDoList.PresensiToDoList.jwt.AccessDenied;
import com.ToDoList.PresensiToDoList.jwt.JwtAuthTokenFilter;
import com.ToDoList.PresensiToDoList.jwt.UnautorizeError;
import com.ToDoList.PresensiToDoList.service.UserDetailSeviselmpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


// Alur pembuatan 19
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

//    menghubungkan ke swagger

    @Autowired
    private AccessDenied accessDeniedHandler;
    @Autowired
    private UnautorizeError unautorizeError;
    @Autowired
    private UserDetailSeviselmpl userDetailService;

    private static final String[] AUTH_WHITLIST =
            {"/v2/api-docs", "/swagger-resources",
            "/swagger-resources/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**", "/swagger-ui/**",
            "/authentication/**", "/", "/user/sign-in",
            "/user/sign-up","/user/{id}","/user/**","/presensi1/masuk","/presensi1/pulang",
                    "/todolist1/post", "/todolist/**","/todolist1/selesai/{id}"
                    ,"/todolist1/{id}", "/presensi1/{id}" ,"/presensi1/**","/presensi1/all-presensi"
                    ,"/todolist/all-todolist","/pulang/post","/pulang/**","/pulang/{id}"
            };

    @Bean
    public JwtAuthTokenFilter authTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().exceptionHandling().authenticationEntryPoint(unautorizeError).and().exceptionHandling().accessDeniedHandler(accessDeniedHandler).and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests().antMatchers(AUTH_WHITLIST).permitAll().anyRequest().authenticated();
        http.addFilterBefore(authTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
