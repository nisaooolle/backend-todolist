package com.ToDoList.PresensiToDoList.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

// Alur pembuatan 20
@Configuration
@EnableWebMvc
public class AppConfig extends WebMvcConfigurerAdapter {

//    untuk membuat, mengelola, dan men-deploy konfigurasi aplikasi dengan cepat.

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*").allowedMethods("*").maxAge(3600);
    }
}
