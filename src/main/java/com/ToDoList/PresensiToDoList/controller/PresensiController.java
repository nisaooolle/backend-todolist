package com.ToDoList.PresensiToDoList.controller;

import com.ToDoList.PresensiToDoList.DTO.PresensiDto;
import com.ToDoList.PresensiToDoList.model.Presensi;
import com.ToDoList.PresensiToDoList.response.CommonResponse;
import com.ToDoList.PresensiToDoList.response.ResponseHelper;
import com.ToDoList.PresensiToDoList.service.PresensiService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// Alur pembuatan 28
@RestController
@RequestMapping("/presensi1")
public class PresensiController {
    //    menjalankan request
    @Autowired
    private PresensiService presensiService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all-presensi")
    public CommonResponse<Page<Presensi>> findByNama(@RequestParam Long userId, Long page) {
        return ResponseHelper.ok(presensiService.findByNama(page, userId));}

    @GetMapping("/{id}")
    public CommonResponse <Presensi> getPresensiById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(presensiService.getPresensiById(id)) ;
    }

    @PostMapping("/post")
    public CommonResponse<Presensi> addPresensi(PresensiDto presensiDto) {
        return ResponseHelper.ok(presensiService.addPresensi(presensiDto));
    }

    @PutMapping("/{id}")
    public CommonResponse<Presensi> editPresensiById(@PathVariable("id") Long id,@RequestBody PresensiDto presensiDto) {
        return ResponseHelper.ok(presensiService.editPresensiById(id, presensiDto));
    }

    @DeleteMapping("/{id}")
    public void deletePresensiById(@PathVariable("id") Long id) { presensiService.deletePresensiById(id);}
}
