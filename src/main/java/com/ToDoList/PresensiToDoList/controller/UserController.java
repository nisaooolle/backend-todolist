package com.ToDoList.PresensiToDoList.controller;

import com.ToDoList.PresensiToDoList.DTO.EdituserDto;
import com.ToDoList.PresensiToDoList.DTO.LoginDto;
import com.ToDoList.PresensiToDoList.DTO.UserDto;
import com.ToDoList.PresensiToDoList.model.User;
import com.ToDoList.PresensiToDoList.response.CommonResponse;
import com.ToDoList.PresensiToDoList.response.ResponseHelper;
import com.ToDoList.PresensiToDoList.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

// Alur pembuatan 18
@RestController
@RequestMapping("/user")
public class UserController {

//    menjalankan request

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all") //untuk melihat semua data
    public CommonResponse<List<User>> getAllUser() {
        return ResponseHelper.ok(userService.getAllUser());
    }

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse <User> getUserById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(userService.getUserById(id)) ;
    }

    @PostMapping("/sign-up") // untuk mengepost data / untuk register
        public CommonResponse<User> addUser(@RequestBody UserDto userDto) {
        return ResponseHelper.ok(userService.addUser(modelMapper.map(userDto, User.class)));
    }

    @PostMapping ("/sign-in") // untuk mengepost data / untuk login
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto) {
        return ResponseHelper.ok(userService.login(loginDto));
    }

    @PutMapping(path = "/{id}",consumes = "multipart/form-data") // untuk mengedit data sesuai id
    public CommonResponse<User> editUserById(@PathVariable("id") Long id, EdituserDto edituserDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(userService.editUserById(id,modelMapper.map(edituserDto, User.class), multipartFile));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public void deleteUSerById(@PathVariable("id") Long id) { userService.deleteUserById(id);}
}
