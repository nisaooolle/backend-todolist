package com.ToDoList.PresensiToDoList.controller;

import com.ToDoList.PresensiToDoList.DTO.TodoListDto;
import com.ToDoList.PresensiToDoList.model.Presensi;
import com.ToDoList.PresensiToDoList.model.TodoList;
import com.ToDoList.PresensiToDoList.response.CommonResponse;
import com.ToDoList.PresensiToDoList.response.ResponseHelper;
 import com.ToDoList.PresensiToDoList.service.TodoListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// Alur pembuatan 35
@RestController
@RequestMapping("/todolist1")
public class TodoListController {
    //    menjalankan request
    @Autowired
    private TodoListService todoListService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all-todolist") // melihat data sesaui dgn userId
    public CommonResponse<List<TodoList>> findByNama(@RequestParam Long userId) {
        return ResponseHelper.ok(todoListService.findByNama(userId));}

    @GetMapping("/{id}")
    public CommonResponse <TodoList> getTodoListById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(todoListService.getTodoListById(id)) ;
    }

    @PostMapping("/post")
    public CommonResponse<TodoList> addTodoList(@RequestBody TodoListDto todoListDto) {
        return ResponseHelper.ok(todoListService.addTodoList(todoListDto));
    }

    @PutMapping("/{id}")
    public CommonResponse<TodoList> editTodoListById(@PathVariable("id") Long id,@RequestBody TodoListDto todoListDto) {
        return ResponseHelper.ok(todoListService.editTodoListById(id,todoListDto));
    }

    @PutMapping("/selesai/{id}")
    public CommonResponse<TodoList>selesai(@PathVariable("id") Long id){
        return ResponseHelper.ok(todoListService.selesai(id));
    }

    @DeleteMapping("/{id}")
    public void deleteTodoListById(@PathVariable("id") Long id) { todoListService.deleteTodoListById(id);}
}
