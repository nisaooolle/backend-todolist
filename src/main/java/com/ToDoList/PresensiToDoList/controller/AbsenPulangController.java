package com.ToDoList.PresensiToDoList.controller;

import com.ToDoList.PresensiToDoList.DTO.AbsenPulangDto;
import com.ToDoList.PresensiToDoList.model.AbsenPulang;
import com.ToDoList.PresensiToDoList.response.CommonResponse;
import com.ToDoList.PresensiToDoList.response.ResponseHelper;
import com.ToDoList.PresensiToDoList.service.AbsenPulangService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

// Alur pembuatan 39
@RestController
@RequestMapping("/pulang")
public class AbsenPulangController {

    //    menjalankan request

    @Autowired
    private AbsenPulangService absenPulangService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<Page<AbsenPulang>> findByNama(@RequestParam Long userId, Long page) {
        return ResponseHelper.ok(absenPulangService.findByNama(page, userId));}

    @GetMapping("/{id}")
    public CommonResponse <AbsenPulang> getAbsen(@PathVariable("id")Long id) {
        return ResponseHelper.ok(absenPulangService.getAbsen(id)) ;
    }

    @PostMapping("/post")
    public CommonResponse<AbsenPulang> addAbsen(AbsenPulangDto absenPulangDto) {
        return ResponseHelper.ok(absenPulangService.addAbsen(absenPulangDto));
    }

    @PutMapping("/{id}")
    public CommonResponse<AbsenPulang> editAbsen(@PathVariable("id") Long id,@RequestBody AbsenPulangDto absenPulangDto) {
        return ResponseHelper.ok(absenPulangService.editAbsen(id, absenPulangDto));
    }

    @DeleteMapping("/{id}")
    public void deleteAbsen(@PathVariable("id") Long id) { absenPulangService.deleteAbsen(id);}
}
