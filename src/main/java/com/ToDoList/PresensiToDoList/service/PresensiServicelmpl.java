package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.PresensiDto;
import com.ToDoList.PresensiToDoList.Exception.NotFoundException;
import com.ToDoList.PresensiToDoList.model.Presensi;
import com.ToDoList.PresensiToDoList.repository.PresensiRepository;
import com.ToDoList.PresensiToDoList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


// Alur pembuatan 27
@Service
public class PresensiServicelmpl implements PresensiService{
    //    untuk memanggil method implementsnya
    @Autowired
    PresensiRepository presensiRepository;

    @Autowired
    UserRepository userRepository;

    private static final Integer HOUR =3600 * 1000;
    @Override
    public Presensi addPresensi(PresensiDto presensiDto) {
        Presensi presensi1 = new Presensi();
        presensi1.setStatus(presensiDto.getStatus());
        presensi1.setMasuk(new Date(new Date().getTime() + 7 * HOUR));
        presensi1.setUserId(userRepository.findById(presensiDto.getUserId()).orElseThrow(() -> new NotFoundException("ERROR")));
//        presensi1.setRole(PresensiType.MASUK);
        return presensiRepository.save(presensi1);
    }

//    @Transactional
//    @Override
//    public Presensi addAbsen(PresensiDto presensiDto) {
//        Presensi presensi1 = new Presensi();
//        presensi1.setStatus(presensiDto.getStatus());
//        presensi1.setMasuk(new Date(new Date().getTime() + 7 * HOUR));
//        presensi1.setUserId(userRepository.findById(presensiDto.getUserId()).orElseThrow(() -> new NotFoundException("ERROR")));
//        presensi1.setRole(PresensiType.PULANG);
//        return presensiRepository.save(presensi1);
//    }
    @Override
    public Presensi getPresensiById(Long id) {
        return presensiRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));

    }
    @Override
    public Page<Presensi> findByNama(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return presensiRepository.findByNama(userId, pageable);}

    @Override
    public Presensi editPresensiById(Long id, PresensiDto presensiDto) {
        Presensi presensi1 = presensiRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak ada"));
        presensi1.setStatus(presensiDto.getStatus());
        return presensiRepository.save(presensi1);
    }

    @Override
    public Map<String, Boolean> deletePresensiById(Long id) {
        try {
            presensiRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
