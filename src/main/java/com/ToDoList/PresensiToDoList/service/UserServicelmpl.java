package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.LoginDto;
import com.ToDoList.PresensiToDoList.Exception.InternalErrorException;
import com.ToDoList.PresensiToDoList.Exception.NotFoundException;
import com.ToDoList.PresensiToDoList.enumeted.UserType;
import com.ToDoList.PresensiToDoList.jwt.JwtProvider;
import com.ToDoList.PresensiToDoList.model.User;
import com.ToDoList.PresensiToDoList.repository.UserRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServicelmpl implements UserService{
//    Alur pembuatan 4 SERVICELMPL User

    //    untuk memanggil method implementsnya
    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/todolist-ba541.appspot.com/o/%s?alt=media";

    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(),loginDto.getPassword());
        User user1 = userRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token" , token);
        response.put("expired", "15 menit");
        response.put("user" , user1);
        return response;
    }
    private String authories(String email, String password) {  //menghandle error saat email/password salah
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        }catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public User addUser(User user) { // menambahkan data
        String email = user.getEmail();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(UserType.USER);
        var validasi = userRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        return userRepository.save(user);
    }

    private String convertToBase64Url(MultipartFile file) { //untuk mengepost file
        String url = "";
        try{
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            return  url;
        }

    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtentions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file,fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private  File convertToFile(MultipartFile multipartFile,String fileName) throws IOException {
        File file = new File(fileName);
        try(FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException { //mengaploud file
        BlobId blobId = BlobId.of("todolist-ba541.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtentions(String fileName) {
        return fileName.split("\\.")[0];
    }

    @Override
    public User getUserById(Long id) { // method untuk melihat sesuaiid yg dicari
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }// method untuk melihat semua data

    @Transactional
    @Override
    public User editUserById(Long id, User user, MultipartFile multipartFile) { // method untuk mengedit data sesuai id
        String url = convertToBase64Url(multipartFile);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User update = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));

        update.setPassword(user.getPassword());
        update.setNama(user.getNama());
        update.setAlamat(user.getAlamat());
        update.setNoTlpn(user.getNoTlpn());
        update.setFotoProfile(url);
        return update;
    }

    @Override
    public Map<String, Boolean> deleteUserById(Long id) {   // method untuk mendelete data sesaui data
        try {
            userRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
