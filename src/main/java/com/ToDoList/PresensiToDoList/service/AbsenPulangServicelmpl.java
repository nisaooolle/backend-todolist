package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.AbsenPulangDto;
import com.ToDoList.PresensiToDoList.Exception.NotFoundException;
import com.ToDoList.PresensiToDoList.model.AbsenPulang;
import com.ToDoList.PresensiToDoList.repository.AbsenPulangRepository;
import com.ToDoList.PresensiToDoList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AbsenPulangServicelmpl implements AbsenPulangService{

    //    untuk memanggil method implementsnya
    @Autowired
    UserRepository userRepository;

    @Autowired
    AbsenPulangRepository absenPulangRepository;

    private static final Integer HOUR =3600 * 1000;

    @Transactional
    @Override
    public AbsenPulang addAbsen(AbsenPulangDto absenPulangDto) {
        AbsenPulang absenPulang1 = new AbsenPulang();
        absenPulang1.setPulang(new Date(new Date().getTime() + 7 * HOUR));
        absenPulang1.setUserId(userRepository.findById(absenPulangDto.getUserId()).orElseThrow(() -> new NotFoundException("ERROR")));
//        absenPulang1.setRole(PresensiType.PULANG);
        return absenPulangRepository.save(absenPulang1);
    }

    @Override
    public AbsenPulang getAbsen(Long id) {
        return absenPulangRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));

    }
    @Transactional(readOnly = true)
    @Override
    public Page<AbsenPulang> findByNama(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return absenPulangRepository.findByNama(userId, pageable);
    }

    @Override
    public AbsenPulang editAbsen(Long id, AbsenPulangDto absenPulangDto) {
        AbsenPulang absenPulang1 = absenPulangRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak ada"));
        return absenPulangRepository.save(absenPulang1);
    }

    @Override
    public Map<String, Boolean> deleteAbsen(Long id) {
        try {
            absenPulangRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
