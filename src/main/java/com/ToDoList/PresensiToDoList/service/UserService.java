package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.LoginDto;
import com.ToDoList.PresensiToDoList.DTO.UserDto;
import com.ToDoList.PresensiToDoList.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//    Alur pembuatan 3 SERVICE User
public interface UserService {

//    membuat method

    Map<String, Object> login(LoginDto loginDto);  //untuk login
    User addUser (User user); //method add untuk mengepost
    User getUserById (Long id); // method untuk melihat sesuaiid yg dicari
    List<User> getAllUser(); // method untuk melihat semua data
    User editUserById(Long id, User user, MultipartFile multipartFile); // method untuk mengedit data sesuai id
    Map<String,Boolean> deleteUserById(Long id); // method untuk mendelete data sesaui data
}
