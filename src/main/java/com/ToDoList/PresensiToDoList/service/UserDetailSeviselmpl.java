package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.model.User;
import com.ToDoList.PresensiToDoList.model.UserPriciple;
import com.ToDoList.PresensiToDoList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

// Alur pembuatan 5
@Service
public class UserDetailSeviselmpl implements UserDetailsService {

//    untuk memanggil method implementsnya
    @Autowired
    private UserRepository userRepository; // memanggil repository

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User loginUser = userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("username not found"));
        return UserPriciple.build(loginUser);
    }
}
