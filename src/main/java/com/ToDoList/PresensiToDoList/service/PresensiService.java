package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.AbsenPulangDto;
import com.ToDoList.PresensiToDoList.DTO.PresensiDto;
import com.ToDoList.PresensiToDoList.model.AbsenPulang;
import com.ToDoList.PresensiToDoList.model.Presensi;
import org.springframework.data.domain.Page;

import java.util.Map;

// Alur pembuatan 26
public interface PresensiService {
    //    membuat method
    Presensi addPresensi (PresensiDto presensiDto);
    Presensi getPresensiById (Long id);
    Page<Presensi> findByNama (Long page, Long userId);
    Presensi editPresensiById(Long id, PresensiDto presensiDto);
    Map<String,Boolean> deletePresensiById(Long id);
}
