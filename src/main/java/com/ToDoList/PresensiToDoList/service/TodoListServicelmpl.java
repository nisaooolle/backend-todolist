package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.TodoListDto;
import com.ToDoList.PresensiToDoList.Exception.NotFoundException;
import com.ToDoList.PresensiToDoList.model.Presensi;
import com.ToDoList.PresensiToDoList.model.TodoList;
import com.ToDoList.PresensiToDoList.repository.TodoListRepository;
import com.ToDoList.PresensiToDoList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Alur pembuatan 34
@Service
public class TodoListServicelmpl implements TodoListService{

    //    untuk memanggil method implementsnya
    @Autowired
    TodoListRepository todoListRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public TodoList addTodoList(TodoListDto todoListDto) {
        TodoList todoList1 = new TodoList();
        todoList1.setTask(todoListDto.getTask());
        todoList1.setUserId(userRepository.findById(todoListDto.getUserId()).orElseThrow(() -> new NotFoundException("ERROR")));
        return todoListRepository.save(todoList1);
    }

    @Override
    public TodoList getTodoListById(Long id) {
        return todoListRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public List<TodoList> findByNama(Long userId) {
        return todoListRepository.findByNama(userId);}

    @Override
    public TodoList editTodoListById(Long id, TodoListDto todoListDto) {
        TodoList todoList1 = todoListRepository.findById(id).get();
        todoList1.setTask(todoListDto.getTask());
        return todoListRepository.save(todoList1);
    }

    private static final String selesai="SELESAI";

    @Override
    public TodoList selesai(Long id) {
        TodoList todoList = todoListRepository.findById(id).orElseThrow(()-> new NotFoundException("selesai"));
        todoList.setSelesai(selesai);
        return todoListRepository.save(todoList);
    }

    @Override
    public Map<String, Boolean> deleteTodoListById(Long id) {
        try {
            todoListRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

}
