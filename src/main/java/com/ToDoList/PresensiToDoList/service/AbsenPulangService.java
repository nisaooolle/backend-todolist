package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.AbsenPulangDto;
import com.ToDoList.PresensiToDoList.DTO.PresensiDto;
import com.ToDoList.PresensiToDoList.model.AbsenPulang;
import com.ToDoList.PresensiToDoList.model.Presensi;
import org.springframework.data.domain.Page;

import java.util.Map;

// Alur pembuatan 38
public interface AbsenPulangService {
    //    membuat method
    AbsenPulang addAbsen (AbsenPulangDto absenPulangDto);
    AbsenPulang getAbsen (Long id);
    Page<AbsenPulang> findByNama (Long page, Long userId);
    AbsenPulang editAbsen(Long id, AbsenPulangDto absenPulangDto);
    Map<String,Boolean> deleteAbsen(Long id);
}
