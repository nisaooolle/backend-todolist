package com.ToDoList.PresensiToDoList.service;

import com.ToDoList.PresensiToDoList.DTO.TodoListDto;
import com.ToDoList.PresensiToDoList.model.TodoList;

import java.util.List;
import java.util.Map;

// Alur pembuatan 33
public interface TodoListService {
    //    membuat method

    TodoList addTodoList (TodoListDto todoListDto); //method untuk menamahkan data
    TodoList getTodoListById (Long id);// method untuk melihat sesuaiid yg dicari
    List<TodoList> findByNama (Long userId); //relasi
    TodoList editTodoListById (Long id, TodoListDto todoListDto); // untuk mengedit data sesuai id
    TodoList selesai(Long id); //ceklis data yg sudah selesai sesuai id
    Map<String,Boolean> deleteTodoListById(Long id); // untuk menghapus data sesuai id

}
