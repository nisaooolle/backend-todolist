package com.ToDoList.PresensiToDoList.DTO;

import com.ToDoList.PresensiToDoList.model.User;

// Alur pembuatan 29
public class PresensiDto {
    //    untuk yg penting sj / untuk column yg dpt diisi
    private String status;
    private Long userId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
