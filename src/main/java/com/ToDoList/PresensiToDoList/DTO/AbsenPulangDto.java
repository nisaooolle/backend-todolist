package com.ToDoList.PresensiToDoList.DTO;

import com.ToDoList.PresensiToDoList.model.User;

// Alur pembuatan 41
public class AbsenPulangDto {
    //    untuk yg penting sj / untuk column yg dpt diisi
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
