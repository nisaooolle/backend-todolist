package com.ToDoList.PresensiToDoList.DTO;

import com.ToDoList.PresensiToDoList.model.User;

// Alur pembuatan 40
public class TodoListDto {
    //    untuk yg penting sj / untuk column yg dpt diisi
    private String task;
    private Long userId;

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
