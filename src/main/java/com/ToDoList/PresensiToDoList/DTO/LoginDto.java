package com.ToDoList.PresensiToDoList.DTO;

// Alur pembuatan 23
public class LoginDto {
    //    untuk yg penting sj / untuk column yg dpt diisi
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
