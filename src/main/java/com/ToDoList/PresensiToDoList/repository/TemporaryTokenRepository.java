package com.ToDoList.PresensiToDoList.repository;

import com.ToDoList.PresensiToDoList.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

// Alur pembuatan 30
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken,Long> {
    //    menjalankan query
    Optional<TemporaryToken> findByToken(String token);

    Optional<TemporaryToken> findByUserId(Long userId);
}
