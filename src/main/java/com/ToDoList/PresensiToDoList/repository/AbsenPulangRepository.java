package com.ToDoList.PresensiToDoList.repository;

import com.ToDoList.PresensiToDoList.model.AbsenPulang;
import com.ToDoList.PresensiToDoList.model.Presensi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

// Alur pembuatan 37
public interface AbsenPulangRepository extends JpaRepository<AbsenPulang,Long> {
    //    menjalankan query
    @Query(value = "select * from absen_pulang where user_id = ?1", nativeQuery = true)
    Page<AbsenPulang> findByNama(Long userId, Pageable pageable);
}
