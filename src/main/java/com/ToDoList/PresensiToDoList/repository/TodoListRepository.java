package com.ToDoList.PresensiToDoList.repository;

import com.ToDoList.PresensiToDoList.model.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

// Alur pembuatan 32
public interface TodoListRepository extends JpaRepository<TodoList,Long> {
    //    menjalankan query
    @Query(value = "select * from todolist where user_id = ?1", nativeQuery = true)
    List<TodoList> findByNama(Long userId);
}
