package com.ToDoList.PresensiToDoList.repository;

import com.ToDoList.PresensiToDoList.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//    Alur pembuatan 2 REPOSITORY User
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
//    menjalankan query

    Optional<User> findByEmail(String email);
}
