package com.ToDoList.PresensiToDoList.model;

import com.ToDoList.PresensiToDoList.enumeted.UserType;

import javax.persistence.*;

//    Alur pembuatan 1 MODEL User
@Entity
@Table(name ="user")
public class User {

//    membuat nama column

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "email")
    private String email;

    @Column (name = "password")
    private  String password;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserType role;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Lob
    @Column(name = "foto_profile")
    private String fotoProfile;

    @Column(name = "no_tlpn")
    private Integer noTlpn;
    public User() {
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    public User(String email, String password, UserType role, String nama, String alamat, String fotoProfile, Integer noTlpn) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.nama = nama;
        this.alamat = alamat;
        this.fotoProfile = fotoProfile;
        this.noTlpn = noTlpn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFotoProfile() {
        return fotoProfile;
    }

    public void setFotoProfile(String fotoProfile) {
        this.fotoProfile = fotoProfile;
    }

    public Integer getNoTlpn() {
        return noTlpn;
    }

    public void setNoTlpn(Integer noTlpn) {
        this.noTlpn = noTlpn;
    }
}
