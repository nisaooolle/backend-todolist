package com.ToDoList.PresensiToDoList.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

// Alur pembuatan 21
public class UserPriciple implements UserDetails {

    private  String email;
    private  String passowrd;

    private Collection<? extends  GrantedAuthority> authority;

    public UserPriciple(String email, String passowrd, Collection<? extends GrantedAuthority> authority) {
        this.email = email;
        this.passowrd = passowrd;
        this.authority = authority;
    }

    //  UserPrinciple memanggil method build
    public static  UserPriciple build(User user) {
        var role = Collections.singletonList(new SimpleGrantedAuthority(user.getRole().name()));
        return new UserPriciple(
                user.getEmail(),
                user.getPassword(),
                role
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    @Override
    public String getPassword() {
        return passowrd;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
