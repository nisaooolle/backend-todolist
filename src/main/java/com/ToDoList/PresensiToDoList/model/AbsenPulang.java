package com.ToDoList.PresensiToDoList.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

// Alur pembuatan 36
@Entity
@Table(name = "Absen_pulang")
public class AbsenPulang {
    //    membuat nama column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Enumerated(value = EnumType.STRING)
//    @Column(name = "role")
//    private PresensiType role;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "pulang", updatable = false)
    private Date pulang;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    public AbsenPulang() {
    }

    public AbsenPulang(Date pulang, Date tanggal, User userId) {
        this.pulang = pulang;
        this.tanggal = tanggal;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPulang() {
        return pulang;
    }

    public void setPulang(Date pulang) {
        this.pulang = pulang;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

}
