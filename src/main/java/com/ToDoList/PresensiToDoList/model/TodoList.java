package com.ToDoList.PresensiToDoList.model;

import javax.persistence.*;

// Alur pembuatan 31
@Entity
@Table(name = "todolist")
public class TodoList {
    //    membuat nama column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "task")
    private String task;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    @Column(name = "selesai")
    private String selesai;

    public TodoList() {
    }

    public TodoList(String task) {
        this.task = task;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getSelesai() {
        return selesai;
    }

    public void setSelesai(String selesai) {
        this.selesai = selesai;
    }
}
