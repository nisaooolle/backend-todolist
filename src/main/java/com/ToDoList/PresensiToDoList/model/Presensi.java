package com.ToDoList.PresensiToDoList.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

// Alur pembuatan 24
@Entity
@Table(name = "presensi")
public class Presensi {
    //    membuat nama column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "masuk")
    private Date masuk;

//    @Enumerated(value = EnumType.STRING)
//    @Column(name = "role")
//    private PresensiType role;

    @Column(name = "status")
    private String status;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal")
    private Date tanggal;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

    public Presensi() {
    }

    public Presensi(Date masuk, String status, Date tanggal, User userId) {
        this.masuk = masuk;
        this.status = status;
        this.tanggal = tanggal;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Date getMasuk() {
        return masuk;
    }

    public void setMasuk(Date masuk) {
        this.masuk = masuk;
    }


}
